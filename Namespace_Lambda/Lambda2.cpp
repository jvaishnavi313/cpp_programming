#include<iostream>

int main(){
	
	int a=10;
	std::string name="main";
/*
	auto add=[=](int x, int y)->int{                                 // = -> all variables captured by value
		std::cout<<a<<": "<<name<<std::endl;
		return x+y;
	};
	auto add=[&](int x, int y)->int{                              // & -> all variables captured by reference
		std::cout<<a<<": "<<name<<std::endl;               
		return x+y;
	};
	auto add=[a](int x, int y)->int{                             //specific variable capture
		std::cout<<a<<std::endl;
		return x+y;
	};*/
	auto add=[a,&name](int x, int y)->int{                      // can capture both by value & reference
		std::cout<<a<<": "<<name<<std::endl;
		return x+y;
	};

	
	std::cout<<add(10,20)<<std::endl;

	return 0;
}
