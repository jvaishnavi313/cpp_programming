//STACK : LIFO 
#include<iostream>
#include<stack>

int main(){
	
	//std::stack<int> s =  {10,20,30,10,20,300};   //error: could not convert ‘{10, 20, 30, 10, 20, 300}’ from ‘<brace-enclosed initializer list>’ to ‘std::stack<int>’
	//std::stack<int>::iterator i;                 //error: ‘iterator’ is not a member of ‘std::stack<int>’
	
	std::stack<int> s;

	s.push(10);
	s.push(40);
	s.push(10);
	s.push(34);
	s.push(1);
	s.push(3);
	
	int var=s.size();

	for(int i=0;i<var;i++){
		std::cout<<s.top()<<std::endl;
		s.pop();
	}	
	return 0;
}
