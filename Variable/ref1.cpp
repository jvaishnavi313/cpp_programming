#include<iostream>

int main(){
	
	int x=10;            

	//int &y;         //error: ‘y’ declared as reference but not initialized

	int &y=x;
	
	std::cout<<&x<<std::endl;                    //0x7ffc98981c9c
	std::cout<<&y<<std::endl;                    //0x7ffc98981c9c
	return 0;

}
