#include<iostream>

class Demo{
	int x=10;

	public:
	Demo(){
		std::cout<<"No-args constructor"<<std::endl;
		std::cout<<x<<std::endl;
	}
	Demo(int x){
		this->x=x;
		std::cout<<"Para constructor"<<std::endl;
		std::cout<<x<<std::endl;
		Demo();
	}
	~Demo(){
		std::cout<<"Destructor"<<std::endl;
	}
	void getData(){
		std::cout<<x<<std::endl;
	}
};

int main(){
	
	Demo obj1(50);             
	std::cout<<"End Main"<<std::endl;

	return 0;
}
