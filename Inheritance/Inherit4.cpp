#include<iostream>

class Parent{

	public:
	Parent(){
		std::cout<<"Parent Constructor"<<std::endl;
	}
	~Parent(){
		std::cout<<"Parent Destructor"<<std::endl;
	}
};

class Child : public Parent{                          
	
	public:
	
	Child(){
		std::cout<<"Child Constructor"<<std::endl;
	}
	~Child(){
		std::cout<<"Child Destructor"<<std::endl;
	}

	friend void* operator new(size_t size){
		std::cout<<"In new"<<std::endl;
		void *ptr= malloc(size);
		return ptr;
	}

	void operator delete(void* ptr){
		std::cout<<"In delete"<<std::endl;
		free(ptr);
	}
};

int main(){

	Child *obj = new Child();
	//operator new(size_t(child))
	//obj=ptr
	//Child(obj)
	//constructor   =>1)Parent Constructor
	              //=>2)Child COnstructor


	delete obj;                       
	//notify
	// Destructor   =>1) child destructor =>2) Parent destructor
	//Operator delete

	return 0;
}
