#include<iostream>

//diamond problem

/*
class Demo{
	public:
		void getData(){
			std::cout<<"Demo getDAta"<<std::endl;
		}
};

class DemoChild1 : public Demo{};
class DemoChild2 : public Demo{};


class Child : public DemoChild1, public DemoChild2{};

int main(){
	Child obj;
	obj.getData();          //error: request for member ‘getData’ is ambiguous

}
*/
//to solve diamond problem in cpp unsing virtual base class

class Demo{
	public:
		void getData(){
			std::cout<<"Demo getDAta"<<std::endl;
		}
};

class DemoChild1 : virtual public Demo{};
class DemoChild2 : virtual public Demo{};


class Child : public DemoChild1, public DemoChild2{};

int main(){
	Child obj;
	obj.getData();          
}
