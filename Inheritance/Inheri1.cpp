#include<iostream>

class Parent{
	int x=10;

	protected:
	int y=20;

	public:
	int z=30;

	void getData(){
		std::cout<<x<<" "<<y<<" "<<z<<std::endl;
	}

	Parent(int x,int y,int z){
		this->x=x;
		this->y=y;
		this->z=z;
	}

	Parent(){
	}
};

class Child : Parent{                               //default private inheritance
	
	public:
	void getInfo(){
		std::cout<<y<<" "<<z<<std::endl;
		getData();
	}

	Child(int x,int y,int z){
		Parent(x,y,z);                     //here create new object of parent
	}
	Child(){}
};

int main(){
	
	Child obj(100,200,300);
	obj.getInfo();
	return 0;
}
