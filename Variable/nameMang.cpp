#include<iostream>

int add(int x,int y){                        //addii() or add2i()
	return x+y;
}

int add(int x,int y,int z){                        //addiii() or add3i()
	return x+y+z;
}

int main(){
	
	std::cout<<add(10,20)<<std::endl;
	std::cout<<add(10,20,30)<<std::endl;
	return 0;
}
