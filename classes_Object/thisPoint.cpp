#include<iostream>

class Demo{
	int x=10;
	int y=20;

	public:
	void info(){
		std::cout<<this<<std::endl;             //0x100
		std::cout<<this->x<<std::endl;          //10
		std::cout<<this->y<<std::endl;          //20
	}
};

int main(){
	Demo obj;

	//std::cout<<obj<<std::endl;   //error: no match for ‘operator<<’ (operand types are ‘std::ostream’ {aka ‘std::basic_ostream<char>’} and ‘Demo’)

	std::cout<<&obj<<std::endl;             //0x100
	obj.info();
	return 0;
}
