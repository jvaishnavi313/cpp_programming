#include<iostream>

class Demo{

	public:
	Demo(){		
		std::cout<<"No-args constructor"<<std::endl;
	}
	Demo(int x){		
		std::cout<<"Para constructor"<<std::endl;
		std::cout<<x<<std::endl;
	}
	
};

int main(){
	Demo obj;                   //no-args               
	Demo *obj2=new Demo();      //no-args             
	
	Demo obj3(10);                  //para =>10
	Demo *obj4 = new Demo(20);      //para =>20
					//
	Demo obj5{};                    //no-args
	Demo obj6();                    //no call for constructor //function declaration
	return 0 ;
}
