#include<iostream>

class Parent{
	int x=10;
	int y=20;
	public:
	Parent(){
		std::cout<<"This in parent"<<this<<std::endl;
		std::cout<<"Parent Constructor"<<std::endl;
	}

	Parent(int x,int y){
		this->x=x;
		this->y=y;
		std::cout<<"In para Parent"<<std::endl;
		std::cout<<"this para Parent : "<<this<<std::endl;
	}
	
	void getData(){
		std::cout<<x<<" "<<y<<std::endl;
	}
};

class Child : public Parent{                          
	int z=30;
	public:
	
	Child(int x,int y,int z){
		std::cout<<"Child Constructor"<<std::endl;
		std::cout<<"this in child : "<<this<<std::endl;
		Parent(x,y);                     //here new object (temp) created
	}
	
	void printData(){
		std::cout<<z<<std::endl;
	}
};

int main(){

	Child obj(40,50,60);
	obj.getData();
	obj.printData();

	return 0;
}
