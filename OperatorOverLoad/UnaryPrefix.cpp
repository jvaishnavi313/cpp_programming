#include<iostream>

class Demo{
	
	int x=10;
	public:
	int operator++(){
		return ++x;
	}
	/*
	friend int operator++(Demo& obj){
		return ++obj.x;
	}*/

	int operator--(){
		return --x;
	}
	/*
	friend int operator--(Demo& obj){
		return --obj.x;
	}*/

	int getX(){
		return ++x;
	}

	int GetMinus(){
		return --x;
	}
};

int main(){
	Demo obj;
	std::cout<<++obj<<std::endl;
	std::cout<<--obj<<std::endl;
}
