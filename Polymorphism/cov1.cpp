#include<iostream>
class Parent{
	public:
		virtual Parent*  getData(){
			std::cout<<"Parent getData"<<std::endl;
			//return new Parent();
			return this;
		
		}
};

class child:public Parent{
	public:
		child* getData(){

			std::cout<<"child getData"<<std::endl;
			//return new child();
			return this;
		
		}
};

int main(){
	Parent *obj=new child();
	obj->getData();
	return 0;
}
