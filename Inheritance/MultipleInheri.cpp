#include<iostream>

class Parent1{
	public:
	Parent1(){
		std::cout<<"Parent Constrcutor - 1"<<std::endl;	
	}
};

class Parent2{
	public:
	Parent2(){
		std::cout<<"Parent Constrcutor - 2"<<std::endl;	
	}
};

class Child : public Parent1,public Parent2{
	public:
	Child(){
		std::cout<<"Child Constrcutor"<<std::endl;	
	}
};

int main(){
	Child obj;
	return 0;

	//Parent Constrcutor - 1
	//Parent Constrcutor - 2
	//Child Constrcutor
}
