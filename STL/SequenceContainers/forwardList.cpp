#include<iostream>
#include<forward_list>

int main(){
	
	std::forward_list<int> fw={10,20,30,40,50};

	for(int& data:fw){
		
		std::cout<<data<<std::endl;
	}

	std::forward_list<int>::iterator itr;
	
	//1.insert_after
		
	std::cout<<"After Insert_after: ";
	fw.insert_after(fw.before_begin(),5);

	for(itr=fw.begin();itr!=fw.end();itr++){
		std::cout<<*itr<<" ";
	}std::cout<<std::endl;
	
	//2.erase_after
	fw.erase_after(fw.begin());

	std::cout<<"AFter erase_after: ";
	for(itr=fw.begin();itr!=fw.end();itr++){
		std::cout<<*itr<<" ";
	}std::cout<<std::endl;
	return 0;
}
