#include<iostream>

class Demo{

	public:
	int x=10;
	Demo(){
		std::cout<<"No-args constructor"<<std::endl;
	}
	
	void getData(){                   
		std::cout<<x<<std::endl;
	}
};

int main(){
	
	const Demo obj;                         
       	obj.getData();                    //error: passing ‘const Demo’ as ‘this’ argument discards qualifiers
	obj.x=80;                         // error: assignment of member ‘Demo::x’ in read-only object
 	
	return 0;
}
