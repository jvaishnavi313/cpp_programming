#include<iostream>
#include<vector>

class Player{
	
	int jerNo;
	std::string pName;

	public:
	Player(int jerNo,std::string pName){
		this->jerNo = jerNo;
		this->pName = pName;
	}

	void info(){
		
		std::cout<<"jerNo: "<<jerNo<<" "<<"Name: "<<pName<<std::endl;
	}
};

int main(){
	
	Player pOne(18,"virat");
	Player pTwo(7,"MSD");
	Player pThree(45,"Rohit");

	std::vector<Player> obj={pOne,pTwo,pThree};

	for(int i=0;i<obj.size();i++){
		obj[i].info();
	}
	
	return 0;
}
