//QUEUE : FIFO => insertion order preserve
#include<iostream>
#include<queue>

void showQueue(std::queue<int>& obj){
		
	int var=obj.size();
	for(int i=0;i<var;i++){
		std::cout<<obj.front()<<std::endl;
		obj.pop();
	}
}
int main(){
	
	//std::queue<int> que ={10,20,3,40};                    //error: could not convert
	std::queue<int> que;     
	//std::queue<int>::iterator itr; 	               //error: ‘iterator’ is not a member of ‘std::queue<int>’
	
	que.push(10);
	que.push(30);
	que.push(20);
	que.push(10);
	que.push(40);

	showQueue(que);
	return 0;
}
