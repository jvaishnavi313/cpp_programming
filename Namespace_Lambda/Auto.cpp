#include<iostream>
#include<functional>
int main(){
	
	int a=10;
	std::string name="main";

	std::function<int(int,int)> add=[=](int x, int y){                                 // = -> all variables captured by value
		
		std::cout<<a<<": "<<name<<std::endl;
		return x+y;
	};
	
	
	std::cout<<add(10,20)<<std::endl;
	return 0;
}
