#include<iostream>

class Demo{
	public:
	int x=10;
	Demo(){
		Demo(this);               //error: invalid conversion from ‘Demo*’ to ‘int’
		std::cout<<"No-args constructor"<<std::endl;
	}
	Demo(int x){
		Demo();                    
		std::cout<<"Para constructor"<<std::endl;
	}
	Demo(Demo& obj){
		std::cout<<"Copy"<<std::endl;
	}
	
};

int main(){
	
	Demo obj1(10);
	Demo obj2(obj1);
	return 0;
}
