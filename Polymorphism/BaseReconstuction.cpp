#include<iostream>

class Demo{
	public:
		Demo(){
			std::cout<<"COnstrction Demo"<<std::endl;
		}
};

class ChildDemo1 : virtual public Demo{
	
	public:
	ChildDemo1(){
		std::cout<<"ChildDemo-1"<<std::endl;
	}
};
class ChildDemo2 : virtual public Demo{
	
	public:
	ChildDemo2(){
		std::cout<<"ChildDemo-2"<<std::endl;
	}
};

class Child : public ChildDemo1, public ChildDemo2{
	
};

int main(){
	Child obj;
}
