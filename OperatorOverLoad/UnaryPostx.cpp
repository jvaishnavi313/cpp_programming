#include<iostream>

class Demo{
	
	int x=10;
	public:

	int operator++(){
		return ++x;
	}

	int operator++(int){
		int temp=x;
		++x;
		return temp;
	}

	friend std::ostream& operator<<(std::ostream& out,const Demo& obj){
		out<<obj.x;
		return out;
	}
};

int main(){
	Demo obj;

	std::cout<<++obj<<std::endl;
	std::cout<<obj++<<std::endl;
	std::cout<<obj<<std::endl;

	return 0 ;
}
