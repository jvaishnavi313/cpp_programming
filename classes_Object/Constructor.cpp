#include<iostream>

class Demo{

        public:
        Demo(){
            std::cout<<"in demo"<<std::endl;  
        }

        Demo(int x){
            std::cout<<"in demo(int x)"<<std::endl;
            
        }

        Demo(int x ,int y){
        }

        Demo(Demo &ref){
            std::cout<<"in copy"<<std::endl;
        }
      
};

int main(){
    
    Demo obj1;
    // Demo obj5(obj1);    or   Demo obj5 = obj1;              // call for copy constructor

    Demo *obj2 = new Demo(10);

    Demo *obj3 = new Demo(10,20);

    Demo arr[2]= {obj1,obj1};            // call for copy constructor

    Demo obj5{obj1};                     // call for copy constructor



    

   

}
