#include<iostream>
#include<vector>

int main(){
	
	std::vector<int> v = {1,2,3,4,5};
	
	//begin() and end()
	for(auto i=v.begin();i!=v.end();i++){
		std::cout<<*i<<" ";
	}
	std::cout<<std::endl;
	
	//cbegin() and cend()              constant iterator 
	for(auto i=v.cbegin();i!=v.cend();i++){
		//*i=100;                             error: assignment of read-only location
		std::cout<<*i<<" ";
	}
	std::cout<<std::endl;
	
	//rbegin() and rend()                 reverse Begin
	for(auto i=v.rbegin();i!=v.rend();i++){
		std::cout<<*i<<" ";
	}
	std::cout<<std::endl;
	
	//crbegin() and crend()                constant reverse   
	for(auto i=v.crbegin();i!=v.crend();i++){
		std::cout<<*i<<" ";
		//*i=100;                             error: assignment of read-only location
	}
	std::cout<<std::endl;
	return 0;

}
