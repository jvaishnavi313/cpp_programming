	.file	"vptr.cpp"
	.text
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.rodata
.LC0:
	.string	"IN getData"
	.section	.text._ZN6parent7getDataEv,"axG",@progbits,_ZN6parent7getDataEv,comdat
	.align 2
	.weak	_ZN6parent7getDataEv
	.type	_ZN6parent7getDataEv, @function
_ZN6parent7getDataEv:
.LFB1731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, %rsi
	leaq	_ZSt4cout(%rip), %rax
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1731:
	.size	_ZN6parent7getDataEv, .-_ZN6parent7getDataEv
	.section	.text._ZN6parent7setDataEv,"axG",@progbits,_ZN6parent7setDataEv,comdat
	.align 2
	.weak	_ZN6parent7setDataEv
	.type	_ZN6parent7setDataEv, @function
_ZN6parent7setDataEv:
.LFB1732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, %rsi
	leaq	_ZSt4cout(%rip), %rax
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1732:
	.size	_ZN6parent7setDataEv, .-_ZN6parent7setDataEv
	.section	.text._ZN6parent9ptinyDataEv,"axG",@progbits,_ZN6parent9ptinyDataEv,comdat
	.align 2
	.weak	_ZN6parent9ptinyDataEv
	.type	_ZN6parent9ptinyDataEv, @function
_ZN6parent9ptinyDataEv:
.LFB1733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, %rsi
	leaq	_ZSt4cout(%rip), %rax
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1733:
	.size	_ZN6parent9ptinyDataEv, .-_ZN6parent9ptinyDataEv
	.section	.text._ZN6parent6swDataEv,"axG",@progbits,_ZN6parent6swDataEv,comdat
	.align 2
	.weak	_ZN6parent6swDataEv
	.type	_ZN6parent6swDataEv, @function
_ZN6parent6swDataEv:
.LFB1734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, %rsi
	leaq	_ZSt4cout(%rip), %rax
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1734:
	.size	_ZN6parent6swDataEv, .-_ZN6parent6swDataEv
	.section	.text._ZN6parent6PNDataEv,"axG",@progbits,_ZN6parent6PNDataEv,comdat
	.align 2
	.weak	_ZN6parent6PNDataEv
	.type	_ZN6parent6PNDataEv, @function
_ZN6parent6PNDataEv:
.LFB1735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, %rsi
	leaq	_ZSt4cout(%rip), %rax
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1735:
	.size	_ZN6parent6PNDataEv, .-_ZN6parent6PNDataEv
	.text
	.globl	main
	.type	main, @function
main:
.LFB1736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTV6parent(%rip), %rax
	movq	%rax, -32(%rbp)
	movl	$10, -24(%rbp)
	movl	$20, -20(%rbp)
	movl	$0, %eax
	movq	-8(%rbp), %rdx
	subq	%fs:40, %rdx
	je	.L8
	call	__stack_chk_fail@PLT
.L8:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1736:
	.size	main, .-main
	.weak	_ZTV6parent
	.section	.data.rel.ro.local._ZTV6parent,"awG",@progbits,_ZTV6parent,comdat
	.align 8
	.type	_ZTV6parent, @object
	.size	_ZTV6parent, 56
_ZTV6parent:
	.quad	0
	.quad	_ZTI6parent
	.quad	_ZN6parent7getDataEv
	.quad	_ZN6parent7setDataEv
	.quad	_ZN6parent9ptinyDataEv
	.quad	_ZN6parent6swDataEv
	.quad	_ZN6parent6PNDataEv
	.weak	_ZTI6parent
	.section	.data.rel.ro._ZTI6parent,"awG",@progbits,_ZTI6parent,comdat
	.align 8
	.type	_ZTI6parent, @object
	.size	_ZTI6parent, 16
_ZTI6parent:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS6parent
	.weak	_ZTS6parent
	.section	.rodata._ZTS6parent,"aG",@progbits,_ZTS6parent,comdat
	.align 8
	.type	_ZTS6parent, @object
	.size	_ZTS6parent, 8
_ZTS6parent:
	.string	"6parent"
	.text
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
.LFB2237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	cmpl	$1, -4(%rbp)
	jne	.L11
	cmpl	$65535, -8(%rbp)
	jne	.L11
	leaq	_ZStL8__ioinit(%rip), %rax
	movq	%rax, %rdi
	call	_ZNSt8ios_base4InitC1Ev@PLT
	leaq	__dso_handle(%rip), %rax
	movq	%rax, %rdx
	leaq	_ZStL8__ioinit(%rip), %rax
	movq	%rax, %rsi
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rax
	movq	%rax, %rdi
	call	__cxa_atexit@PLT
.L11:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2237:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.type	_GLOBAL__sub_I_main, @function
_GLOBAL__sub_I_main:
.LFB2238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$65535, %esi
	movl	$1, %edi
	call	_Z41__static_initialization_and_destruction_0ii
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2238:
	.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_main
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 11.3.0-1ubuntu1~22.04.1) 11.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	1f - 0f
	.long	4f - 1f
	.long	5
0:
	.string	"GNU"
1:
	.align 8
	.long	0xc0000002
	.long	3f - 2f
2:
	.long	0x3
3:
	.align 8
4:
