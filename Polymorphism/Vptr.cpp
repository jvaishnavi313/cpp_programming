#include<iostream>

class Parent{
	public:
	//void* _vptr;
	
	virtual void getData(){                             
		std::cout<<"Parent GetData"<<std::endl;
	}
	
	virtual void printData(){                          
		std::cout<<"Parent printData"<<std::endl;
	}
};

class Child1 : public Parent{
	
	public:
	void getData(){                                       
		std::cout<<"Child-1 getData"<<std::endl;	
	}
};

class Child2 : public Parent{
	
	public:
	void printData(){                                       
		std::cout<<"Child-2 printData"<<std::endl;	
	}
};


int main(){

	Parent *obj1 = new Child1();
	Parent *obj2 = new Child2();
	
	obj1->getData();                      //obj1-> _vptr ->getData()
	
	obj2->printData();                    //obj2-> _vptr ->printData()
	return 0;
}
