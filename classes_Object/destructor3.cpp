#include<iostream>

class Demo{
	int *ptr=NULL;

	public:
	Demo(){
		ptr=new int[50];
		std::cout<<"No-args constructor"<<std::endl;
	}

	~Demo(){
		delete[] ptr;
		std::cout<<"Destructor"<<std::endl;
	}
	
};

int main(){
	{
	Demo obj1;
	}

	std::cout<<"End Main"<<std::endl;

	return 0;
}
