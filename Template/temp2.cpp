#include<iostream>

template<typename T>
struct Node{
    T data;
    Node* next;
};

template<typename T>
Node<T>* head = nullptr;

template<typename T>
void addNode(T data){

    Node<T>* newNode = new Node<T>;
    newNode->data = data;
    newNode->next = nullptr;

    if(head<T> == nullptr){
        head<T> = newNode;
    }else{

        Node<T>* temp = head<T>;

        while(temp->next != nullptr){
            temp = temp->next;
        }

        temp->next = newNode;
    }
}

template<typename T>
void printList(){

    Node<T>* temp = head<T>;
    while(temp!=nullptr){

        std::cout<<temp->data<<"   ";
        temp = temp->next;
    }

    std::cout<<std::endl;
}
int main(){

    addNode<int>(5);
addNode<int>(5);
addNode<int>(5);
addNode<int>(5);
addNode<char>('A');   

printList<char>();
return 0;
}
