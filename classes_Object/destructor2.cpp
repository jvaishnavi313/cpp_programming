#include<iostream>

class Demo{

	public:
	Demo(){
		std::cout<<"No-args constructor"<<std::endl;
	}

	~Demo(){
		std::cout<<"Destructor"<<std::endl;
	}
	
};

int main(){
	
	Demo obj1;
	Demo *obj2 = new Demo();	
	std::cout<<"End Main"<<std::endl;

	//delete obj1;                     //error: type ‘class Demo’ argument given to ‘delete’, expected pointer

	delete obj2;
	return 0;
}
