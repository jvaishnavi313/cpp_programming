#include<iostream>

int main(){
	//way-1
	
	[](int x,int y){
		std::cout<<x+y<<std::endl;
		
	}(70,80);          //use only once
			   
	//way-2
	auto add=[](int x, int y){
		std::cout<<x+y<<std::endl;
	};

	add(10,20);

	return 0;
}
