//MULTISET: Duplicated data Allowed
//	perform sorting

#include<iostream>
#include<set>                     //included multiset in set

int main(){
	
	std::multiset<int> s = {10,30,40,10,20,30,50};
	std::set<int>::iterator itr;

	for(itr=s.begin();itr!=s.end();itr++){
		std::cout<<*itr<<std::endl;
	}

	//descending order
	std::multiset<int,std::greater<int>> s2={10,30,40,10,20,30,50};
	std::set<int>::iterator it;
	
	std::cout<<"Descending Order: "<<std::endl;
	for(it=s2.begin();it!=s2.end();it++){
		std::cout<<*it<<std::endl;
	}
	return 0;
}
