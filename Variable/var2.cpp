#include<iostream>

int main(){
	
	int x=10;            

	int y = 20.4f;


	std::cout<<x<<std::endl;                    //10
	std::cout<<y<<std::endl;                    //20
	
	int a{10};                                
	int b{20.4f};                          
			    
	std::cout<<a<<std::endl;                   //10
	std::cout<<b<<std::endl;            //error: narrowing conversion of ‘2.03999996e+1f’ from ‘float’ to ‘int'
	return 0;

}
