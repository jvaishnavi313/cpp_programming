//ofstream-constructor
//
#include<iostream>
#include<fstream>

int main(){
	
	std::ofstream outfile("Incubator.txt");  // if file is not present it will create a new file
	outfile<<"Flutter\n";
	outfile<<"BackEnd\n";
	outfile<<"FrontEnd\n";

	//outfile.close();

	std::ofstream outfile1("Incubator.txt",std::ios::app);
	outfile1<<"salesforce\n";
	return 0;

}
