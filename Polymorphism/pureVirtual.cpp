#include<iostream>

class Parent{
	public:
	
	virtual void marray()=0;               //pure virtual function

	virtual void property(){
		std::cout<<"Car,Gold"<<std::endl;
	}
};

void Parent::marray(){
	std::cout<<"Parent choice"<<std::endl;                  //common for all child classes like default method in java
}

class Child:public Parent{
	
	public:
	
	void marray(){
		std::cout<<"Child Choice"<<std::endl;
	}

	//to call Parent marray
	/*
	void marray(){
		Parent::marray();
	}*/
};

int main(){
	
	Parent *obj = new Child();

	obj->property();
	obj->marray();
	return 0;
}
