
#include<iostream>

class Demo{
	int x=10;
	int y=20;

	friend std::istream& operator>>(std::istream& in, Demo& obj){
		in>>obj.x;
		in>>obj.y;
		return in;
	}
	
	public:
	void printData(){
		std::cout<<x<<" "<<y<<std::endl;
	}
};

int main(){
	Demo obj;

	std::cout<<"Enter value"<<std::endl;
	std::cin>>obj;
	obj.printData();

	return 0;
}

//member function cant acheive operator overloading coz first parameter is this and in case of extraction it is istream
