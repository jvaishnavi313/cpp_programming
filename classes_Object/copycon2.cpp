#include<iostream>

class Demo{
	int x=10;
	int y=20;

	public:
	Demo(){
		std::cout<<"No-args constructor"<<std::endl;
	}
	Demo(int x,int y){
		this->x=x;
		this->y=y;
		std::cout<<"Para constructor"<<std::endl;
	}
	Demo(Demo& obj){
		std::cout<<"Copy constructor"<<std::endl;
	}
};

int main(){
	
	Demo obj1;                          //no-args constructor
	
	Demo obj2(1000,2000);               //para constructor
	
	
	Demo obj3(obj1);                    //copy constructor
	
	Demo obj4;                        //no-args constructor
        obj4= obj1;                      //assign only(no call for copy constructor

	return 0;
}
