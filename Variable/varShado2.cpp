#include<iostream>

int y=1000;

int main(){
	
	int x=10;
	std::cout<<x<<std::endl;                    //10
	{
		int x=20;
		std::cout<<x<<std::endl;            //20 
		
		std::cout<<::x<<std::endl;          //error: ‘::x’ has not been declared
		//avoid local variable shadowing
		//
		//::scope resolution operator => only use global variable shadowing

		x=30;
		std::cout<<x<<std::endl;            //30
	}
}
