
#include<iostream>

class Two;            //forward declaration=> optional
class One{
	int x=10;

	protected:
	int y=20;

	public:
	One(){
		std::cout<<"One constructor"<<std::endl;
	}

	
	friend class Two;
};

class Two{

	public:
	Two(){
		std::cout<<"Two constructor"<<std::endl;
	}

	private:
	void getData(const One& obj){
		std::cout<<"x="<<obj.x<<" y="<<obj.y<<std::endl;
	}
	
	public:
	void accessData(const One& obj){
		std::cout<<"x="<<obj.x<<" y="<<obj.y<<std::endl;
		getData(obj);	
	}
};

int main(){
	One obj1;
	Two obj2;

	obj2.accessData(obj1);
	

	return 0;
}
