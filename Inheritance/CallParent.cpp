#include<iostream>


class Parent{
	int x=10;
	
	public:
	Parent(){
		std::cout<<"Parent constructor"<<std::endl;
	}

	void getData(){
	
		std::cout<<"Parent = "<<x<<std::endl;
	}
};

class Child : public Parent{
	int x=20;
	public:
	Child(){
		std::cout<<"Child constructor"<<std::endl;
	}

	void getData(){
		std::cout<<"Child ="<<x<<std::endl;

		Parent::getData();                            //way=>1
	}
};
int main(){
	
	Child obj;
	obj.getData();
	
	(Parent(obj)).getData();                            //Way=>2
	
	obj.Parent::getData();                              //Way=>3
	
	static_cast<Parent&>(obj).getData();                //Way=>4
	return 0;
}
