#include<iostream>

int main(){
	
	int x=10;            //copy intialisation

	int y(20);           //direct intialisation

	int z{30};          //uniform/list/brace intialisation =>  version 11
			    
	std::cout<<x<<std::endl;                  //10
	std::cout<<y<<std::endl;                  //20
	std::cout<<z<<std::endl;                  //30

	return 0;

}
