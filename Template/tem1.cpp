#include<iostream>
#include<stdlib.h>

template<typename T>
struct Node{
	
	T data;
	Node* next;
};

template<typename T>
Node<T>* head =NULL;

template<typename T>
struct Node* createNode(){

        struct Node<T> *newNode = (Node<T>*)malloc(sizeof(Node<T>));

        printf("Enter Data : ");
        scanf("%d",&newNode->data);

        newNode->next = NULL;

        return newNode;
}

template<typename T>
int Count(){
        int count=0;
	if(head == NULL){
                count=0;
        }else{

                struct Node *temp=head;

                 while(temp != NULL){
			count++;	
                        temp = temp -> next;
                  }
        }
	return count;
}
template<typename T>
void PrintNode(){
	
	if(head == NULL){
		printf("LINKEDLIST IS EMPTY\n");
	}else{
       	
	       	struct Node<T> *temp=head<T>;

       		 while(temp != NULL){

                	if(temp -> next != NULL){
                        	printf("| %d |->",temp -> data);
           	     }else{
                	        printf("| %d |", temp -> data);
        	        }

               		 temp = temp -> next;
     	 	  }
	}
}

template<typename T>
void addLast(){
	
	struct Node<T> *newNode = createNode();

	if(head==NULL){
		
		head=newNode;
	}else{
		
		struct Node<T> *temp  = head<T>;

		while(temp->next != NULL){
			temp=temp->next;
		}

		temp->next=newNode;
	}
}
template<typename T>
void addNode(){

	struct Node<T> *newNode = createNode();

	if(head == NULL){
		head=newNode;
	}else{

		struct Node<T> *temp=head<T>;
		while(temp->next != NULL){
			temp = temp->next;
		}

		temp -> next = newNode;
	}
}

template<typename T>
void addFirst(){
	
	struct Node<T> *newNode = createNode();

	if(head == NULL){
		
		head = newNode;
	}else{
		newNode -> next = head;
		head = newNode;
	}
}

template<typename T>
int addAtPos(int pos){
		
		int cnt = Count();

		if(pos<=0 || pos>=cnt+2){
			printf("Invalid Position");
			return -1;

		}else if(pos == cnt+1){
			
			addLast();
		
		}else if(pos == 1){
		
			addFirst();
		}else{
			Node<T> *newNode = createNode();
			Node<T> *temp=head<T>;

			while(pos-2){
				
				temp = temp->next;
				pos--;
			}
			newNode->next = temp->next;
			temp->next=newNode;

		}
}

template<typename T>
void delFirst(){
	
	if(head == NULL){
		printf("LinkedList is Empty\n");
	
	}else{
		
		Node<T> *temp = head<T>;
	
		head<T>=temp->next;
		free(temp);
	}
}

template<typename T>
void delLast(){
	
	if(head == NULL){
		printf("LinkedList is Empty\n");
	
	}
	else if (head->next == NULL){
	
		delFirst();
	}else{
		
		Node<T> *temp = head<T>;
		while(temp->next->next != NULL){
		
			temp = temp->next;
		}

		free(temp->next);
		temp->next=NULL;
	}
	
}

template<typename T>
int delAtPos(int pos){
	
	int count =Count();

	if(pos<=0 || pos>count){
		printf("Invalid Position\n");
		return -1;
	}
	else if(pos==1){
		delFirst();
	}
	else if(pos == count){
		delLast();
	}
	else{
		
		Node<T> *temp1=head<T>;

		while(pos-2){
			
			temp1 = temp1->next;
			pos--;
		}

		struct Node<T> *temp2=temp1->next;

		temp1->next = temp2->next;
		free(temp2);
	}
}
int main(){
	
	char choice;

	do{
		
		printf("1. addNode()\n 2.addFirst()\n 3.addAtPos \n 4.PrintNode\n 5.delFirst()\n 6.delLast()\n 7.delAtPos()\n 8.count()\n");

		int ch;
		printf("Enter choice : ");
		scanf("%d",&ch);

		switch(ch){
			
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:{

				int pos;
				printf("Enter position\n");
				scanf("%d",&pos);
				addAtPos(pos);
			       }
				break;
			case 4: 
				PrintNode();
				break;
			case 5:
				delFirst();
				break;
			case 6:
				delLast();
				break;
			case 7:{
				int pos;
				printf("Enter position\n");
				scanf("%d",&pos);
				delAtPos(pos);
			       }
				break;
			case 8:
				Count();
				break;
			default:
				printf("wrong choice\n");
		}
		
		getchar();
		printf("Do You Want To Continue :  y or n\n");
		scanf("%c",&choice);

	}while(choice == 'y' || choice == 'Y');

    return 0;
}
