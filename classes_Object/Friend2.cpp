//if don't want to change variable in class then pass constant reference
#include<iostream>

class Demo{
	int x=10;

	protected:
	int y=20;

	public:
	Demo(){
		std::cout<<"IN constructor"<<std::endl;
	}

	void getData(){
		std::cout<<"x="<<x<<" y="<<y<<std::endl;
	}

	friend void accessData(const Demo& obj);
};

void accessData(const Demo& obj){
	int temp=obj.x;
	obj.x=obj.y;                    //error: assignment of member ‘Demo::x’ in read-only object

	obj.y=temp;                    //error: assignment of member ‘Demo::y’ in read-only object
}
int main(){
	Demo obj;
	obj.getData();

	accessData(obj);

	obj.getData();

	return 0;
}
