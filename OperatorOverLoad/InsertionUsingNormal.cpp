#include<iostream>

class Demo{
	int x=10;
	int y=20;

	public:
	int getData()const{
		return x;
	}
};

std::ostream& operator<<(std::ostream& out,const Demo& obj){
		out<<obj.getData();
		return out;
}

int main(){
	Demo obj;
	std::cout<<obj<<std::endl;
	return 0;
}
