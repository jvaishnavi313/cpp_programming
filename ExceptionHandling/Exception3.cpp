#include<iostream>

int main(){

    std::cout<<"Start Main"<<std::endl;
    
    try{
    	throw 'A';
    }catch(int x){
    	std::cout<<"Int Exception"<<std::endl;
    }catch(...){
    	std::cout<<"Default Exception"<<std::endl;
    }
    	
    std::cout<<"End Main"<<std::endl;

    return 0;
}
