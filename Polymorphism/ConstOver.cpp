#include<iostream>
class Parent {
    public:
        virtual void getData(int) const{
            std::cout<<"Parent getData"<<std::endl;
        }
     
};

class Child : public Parent {
    public:
	/*
        void getData(int){
            std::cout<<"Child getData"<<std::endl;          //parent getData called

        } */


        void getData(int)const override{
            std::cout<<"Child getData"<<std::endl;

        }
};
int main(){
    Parent *obj=new Child();
    obj->getData(10);
    return 0;
}
