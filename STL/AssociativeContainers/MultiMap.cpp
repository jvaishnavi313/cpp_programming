//MULTIMAP: Key Value Pair            key=> first , value=> second  ( variables )
//	perform sorting
//	if have duplicate key it store duplicate entry

#include<iostream>
#include<map>

int main(){

	
	std::multimap<std::string,std::string> m2={{"Vaish","Jadhav"},{"AK","Malusare"},{"ANI","Navle"},{"Vaish","RJadhav"}};
        std::multimap<std::string,std::string>::iterator i1;

        std::cout<<"WAY-2: "<<std::endl;
        for(i1=m2.begin();i1!=m2.end();i1++){
                std::cout<<i1->first<< ": ";
                std::cout<<i1->second<<std::endl;
        }	

	//Way-3
        std::multimap<int,std::string> player;
        player.insert(std::pair<int,std::string>(10,"Messi"));
        player.insert(std::pair<int,std::string>(7,"Ronaldo"));
        player.insert(std::pair<int,std::string>(7,"RonalDo"));

        std::map<int,std::string>::iterator i3;

        std::cout<<"WAY-3: "<<std::endl;
        for(i3=player.begin();i3!=player.end();i3++){
                std::cout<<i3->first<< ": ";
                std::cout<<i3->second<<std::endl;
        }
	return 0;
}
