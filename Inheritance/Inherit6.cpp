#include<iostream>

class Parent{
	int x=10;
	int y=20;

	public:
	int a=30;
	Parent(){
		std::cout<<"Parent Constructor"<<std::endl;
	}
	~Parent(){
		std::cout<<"Parent Destructor"<<std::endl;
	}

	void getData(){
		std::cout<<x<<" "<<y<<std::endl;
	}
};

class Child : public Parent{                          
	int z=10;
	public:
	
	Child(){
		std::cout<<"Child Constructor"<<std::endl;
	}
	~Child(){
		std::cout<<"Child Destructor"<<std::endl;
	}

	void printData(){
		std::cout<<a<<" "<<z<<std::endl;
	}
};

int main(){

	Child obj;
	obj.printData();
	obj.getData();
	return 0;
}
