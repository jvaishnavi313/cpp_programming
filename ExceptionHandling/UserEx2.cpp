#include<iostream>

class MinimumSal{
	
	std::string msg;
	public:
	MinimumSal(std::string msg){
		this->msg=msg;
	}
	std::string getException(){
		return msg;
	}
};

class MaximumSal{
	
	std::string msg;
	public:
	MaximumSal(std::string msg){
		this->msg=msg;
	}
	std::string getException(){
		return msg;
	}
};
class Employee{
	
	int empId;
	std::string name;
	float sal;

	public:
	Employee(int empId,std::string name,float sal){
		this->empId=empId;
		this->name=name;
		this->sal=sal;
	}

	std::string check(Employee& obj){

		if(obj.sal<5.0f)
			throw MinimumSal("Low Salary");
		else if(obj.sal>15.0f)
			throw MaximumSal("High salary");
		else 
			throw "Avarage Salary";
	}
};

int main(){
	Employee obj1(1,"Vaish",10.5f);
	Employee obj2(2,"Vaishn",3.0f);
	Employee obj3(3,"Jadhav",15.0f);
	
	Employee arr[]={obj1,obj2,obj3};

	try{
		obj1.check(obj2);
	}catch(const char *str){
		std::cout<<str<<std::endl;
	}catch(MinimumSal& obj){
		std::cout<< obj.getException()<<std::endl;
	}catch(MaximumSal& obj2){
		std::cout<<obj2.getException()<<std::endl;
	}

	return 0;

}
