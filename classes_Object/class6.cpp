#include<iostream>

class Player{
	
	int jerNo=18;
	std::string name="virat";
	public:
	void info(){
		std::cout<<jerNo<<" "<<name<<std::endl;
	}
};

int main(){
	Player obj1;
	Player *obj2 = new Player();
	
	obj1.info();                 //info(&obj1)
	obj2->info();                //(*obj2).info() => info(obj2)
	return 0;
}
