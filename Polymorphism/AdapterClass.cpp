#include<iostream>

class IDemo{
	public:
	
	virtual void fun1()=0;
	virtual void fun2()=0;         //pure virtual functions
};

//adapter class

class Adapter : public IDemo{
	public:
		void fun1(){}

		void fun2(){}
};

class child1: public Adapter{
	
	public:
	void fun1(){
		std::cout<<"In child-1"<<std::endl;
	}
};

class child2: public Adapter{
	
	public:
	void fun2(){
		std::cout<<"In child-2"<<std::endl;
	}
};

int main(){
	child1 obj1;
	obj1.fun1();
	
	child2 obj2;
	obj2.fun2();
	return 0;
}

