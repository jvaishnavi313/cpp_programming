#include<iostream>


class Parent{
	int x=10;
	protected:
	int y=20;
	public:
	int z=30;

	void getData(){
		std::cout<<"In getData"<<std::endl;	
	}
};

class Child : public Parent{
	
	using Parent::getData;               //change public to private

	public:
	using Parent::y;                    //change protected to public
};
int main(){
	
	Child obj;

	std::cout<<obj.y<<std::endl;
	std::cout<<obj.z<<std::endl;

	obj.getData();

	//error: ‘void Parent::getData()’ is inaccessible within this context
	return 0;
}
