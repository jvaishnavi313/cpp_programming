#include<iostream>
#include<list>

std::_List_iterator<int>& operator+(std::_List_iterator<int>& lstItr,int index){
	
	while(index){
		lstItr++;
		index--;
	}

	return lstItr;
}
int main(){

	std::list<int> lst = {10,20,30,40,50};
	std::list<int>::iterator itr = lst.begin();
	
	std::cout<<*(++lst.begin())<<std::endl;
	//we can increment pointer return by the begin() method i.e postIncreamet/preIncrement
	
	std::cout<<*(itr+2)<<std::endl;
	
	return 0;
}
