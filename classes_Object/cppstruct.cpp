#include<iostream>

struct Player{
	
	int jerNo =18;
	char name[20] ="virat";          //can intialise value to variable in struct

	void disp(){                      //can write functions inside struct
		
		std::cout<<jerNo<<std::endl;
		std::cout<<name<<std::endl;
	}
};

int main(){
	
	Player obj;                         //no need to write struct keyword
	std::cout<<obj.jerNo << " "<<obj.name<<std::endl;
       
	return 0 ;	
}
