//PRIORITYQUEUE: value display unsing hash values 
//by defulat bucket size is 11
//top-> bottom && right to left

#include<iostream>
#include<queue>

void showQueue(std::priority_queue<int>& obj){
		
	int var=obj.size();
	for(int i=0;i<var;i++){
		std::cout<<obj.top()<<std::endl;
		obj.pop();
	}
}
int main(){
	
	//std::priorityqueue<int> que ={10,20,3,40};                    //error: could not convert
	std::priority_queue<int> que;     
	//std::queue<int>::iterator itr; 	               //error: ‘iterator’ is not a member of ‘std::queue<int>’
	
	que.push(10);
	que.push(30);
	que.push(20);
	que.push(11);
	que.push(10);
	que.push(40);

	showQueue(que);
	return 0;
}
