#include<iostream>


class Parent{
	int x=10;
	
	public:
	Parent(){
		std::cout<<"Parent constructor"<<std::endl;
	}

	void getData(){
	
		std::cout<<"Parent = "<<x<<std::endl;
	}
};

class Child : public Parent{
	int x=20;
	public:
	Child(){
		std::cout<<"Child constructor"<<std::endl;
	}

	void getData(){
		std::cout<<"Child ="<<x<<std::endl;
	}
};
int main(){
	
	Child obj;
	obj.getData();                  //child = 20
					
	Parent obj2;
	obj2.getData();                //parent = 10

	Parent *obj3 = new Child();      
	obj3->getData();               //Parent = 10
	return 0;
}
