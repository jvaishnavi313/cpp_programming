#include<iostream>

class Demo{

	
	int x=10;
	int y=20;
	public:
	Demo(){
		
		std::cout<<"In constructor"<<std::endl;
	}
	void fun(){
		
		std::cout<<x<<std::endl;
		std::cout<<y<<std::endl;
	}
};

int main(){
	Demo obj;                                 //variable type object
	obj.fun();

	Demo *obj2=new Demo();                   //using new keyword
	obj2->fun();
	return 0 ;
}
