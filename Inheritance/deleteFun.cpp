#include<iostream>


class Parent{
	int x=10;
	protected:
	int y=20;
	public:
	int z=30;

	void getData(){
		std::cout<<"In getData"<<std::endl;	
	}
};

class Child : public Parent{
	
	void getData = delete;
};

int main(){
	
	Child obj;
	obj.getData();             //error: variable or field ‘getData’ declared void

	return 0;
}
