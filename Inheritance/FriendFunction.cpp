#include<iostream>


class Parent{
	int x=10;
	
	public:
	Parent(){
		std::cout<<"Parent constructor"<<std::endl;
	}

	friend std::ostream& operator<<(std::ostream& out,const Parent& obj){
		out<<"In Parent"<<std::endl;
		out<<obj.x;
		return out;
	}

	Parent(Parent& obj){
		std::cout<<"Copy"<<std::endl;
	}
};

class Child : public Parent{
	int x=20;
	public:
	Child(){
		std::cout<<"Child constructor"<<std::endl;
	}
	
	friend std::ostream& operator<<(std::ostream& out,const Child& obj){
		out<<"In Child"<<std::endl;
		out<<obj.x;
		return out;
	}
};
int main(){
	
	Child obj;
	
	std::cout<<(const Parent&)obj<<std::endl;

	std::cout<<(const Parent)obj<<std::endl;

	Child obj2;
	std::cout<<obj2<<std::endl;
	return 0;
}
