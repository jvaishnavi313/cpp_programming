//UNORERED MAP : Key VAlues   -> duplicated entry will save
//sorting is not done here
//using hash value it will display output : bucket (11)

#include<iostream>
#include<unordered_map>

int main(){
	
	std::unordered_multimap<int,std::string> s={{1,"vaish"},{1,"ANI"},{4,"AK"},{3,"VRJ"}};
	std::unordered_map<int,std::string>::iterator itr;

	for( itr=s.begin();itr!=s.end();itr++){
		std::cout<<itr->first<<": ";
		std::cout<<itr->second<<std::endl;
	}

	return 0;
}
