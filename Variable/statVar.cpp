#include<iostream>

class Demo{
	
	int x=10;
	//static int y=20;            //error: ISO C++ forbids in-class initialization of non-const static member ‘Demo::y’

	static int y;              // undefined reference to `Demo::y'  error: ld returned 1 exit status

	public:
	void fun(){
		
		std::cout<<x<<std::endl;
		std::cout<<y<<std::endl;
	}
};

int main(){
	
	Demo obj;
	obj.fun();
	return 0;
}
