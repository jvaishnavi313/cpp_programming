#include<iostream>

class Demo{

	public:
	int x=10;
	Demo(){
		this->x=x;
		std::cout<<"No-args constructor"<<std::endl;
	}
	
	void getData()const{                   
		std::cout<<x<<std::endl;
		//x=100;                        //error: assignment of member ‘Demo::x’ in read-only object
	}
};

int main(){
	
	const Demo obj;
	std::cout<<obj.x<<std::endl;	
       	obj.getData();
 	
	return 0;
}
