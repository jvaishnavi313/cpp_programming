//ofstream-constructor
//
#include<iostream>
#include<fstream>

int main(){
	
	std::ofstream outfile("Incubator.txt");  // if file is not present it will create a new file
	outfile<<"Flutter\n";
	outfile<<"BackEnd\n";
	outfile<<"FrontEnd\n";

	outfile.close();

	outfile<<"Salesforce\n";                //no error => handled internally
	return 0;

}
