//copy constructor scenario 4
//Array of objects

#include<iostream>

class Demo{
	public:
	Demo(){
		std::cout<<"No-args constructor"<<std::endl;
	}

	Demo(Demo& obj){
		std::cout<<"Copy constructor"<<std::endl;
	}
	
};

int main(){
	
	Demo obj1;                           //no-args
	Demo obj2;                           //no-args
	Demo obj3;                           //no-args

	Demo arr[]={obj1,obj2,obj3};          //intialiser list
					      
	arr[1]=obj1;                        //no call for copy constructor

	return 0;
}
