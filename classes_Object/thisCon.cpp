#include<iostream>

class Demo{
	public:
	Demo(){
		std::cout<<"No-args constructor"<<std::endl;
	}
	Demo(int x){
		this();                    //error: expression cannot be used as a function
		std::cout<<"Para constructor"<<std::endl;
	}
	
};

int main(){
	
	Demo obj1(10);
	return 0;
}
