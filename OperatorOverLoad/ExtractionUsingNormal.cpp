#include<iostream>

class Demo{
	public:
	int x=10;
	int y=20;

	int getData()const{
		return x;
	}

	void printData(){
		std::cout<<x<<" "<<y<<std::endl;
	}
};

std::istream& operator>>(std::istream& in,Demo& obj){
		in>>obj.x;
		in>>obj.y;
		return in;
}

int main(){
	Demo obj;
	std::cout<<"Enter values"<<std::endl;
	std::cin>>obj;
	obj.printData();
	return 0;
}
