//copy constructor scenario 3
//In constructor 

#include<iostream>

class Demo{
	
	public:
	Demo(){
		std::cout<<"No-args"<<std::endl;
	}
	Demo(int x){
		Demo obj1;
		Demo obj2(obj1);
		
	}

	Demo(Demo& obj){
		std::cout<<"Copy constructor"<<std::endl;
	}
	
};

int main(){
	std::cout<<"IN main"<<std::endl;
	Demo obj5(10);
	return 0;
}
