#include<iostream>

class Demo{
	
	int x=10;            //by default scope private : cannot access from outside

	public:
	int y=20;

	void access();
};

void Demo:: access(){
	std::cout<<"In access"<<std::endl;
}

int main(){
	Demo obj;
	//std::cout<<obj.x<<std::endl;                        // error: ‘int Demo::x’ is private within this context

	obj.access();

	return 0;
}
