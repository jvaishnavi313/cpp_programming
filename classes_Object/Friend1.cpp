#include<iostream>

class Demo{
	int x=10;

	protected:
	int y=20;

	public:
	Demo(){
		std::cout<<"IN constructor"<<std::endl;
	}

	void getData(){
		std::cout<<"x="<<x<<" y="<<y<<std::endl;
	}

	friend void accessData(Demo& obj);
};

void accessData(Demo& obj){
	int temp=obj.x;
	obj.x=obj.y;
	obj.y=temp;
}
int main(){
	Demo obj;
	obj.getData();

	accessData(obj);

	obj.getData();

	return 0;
}
