#include<iostream>

class Demo{
	
	int x=10;
	public:
	friend void* operator new(size_t size){
		std::cout<<"Here"<<std::endl;
		void *ptr=malloc(size);
		return ptr;
	}

	void getData(){
		std::cout<<x<<std::endl;
	}

	void operator delete(void *ptr){
		free(ptr);
		std::cout<<"In delete"<<std::endl;
	}
};

int main(){
	
	Demo *obj=new Demo();

	obj->getData();
	delete obj;
	return 0;
}
