#include<iostream>

class Demo{
	public:
	Demo(){
		std::cout<<"No-args constructor"<<std::endl;
	}
	Demo(int x=10,int y =20){         //default parameters
	
		std::cout<<"Para constructor"<<std::endl;
	}
	
};

int main(){
	
	Demo obj1;             // error: call of overloaded ‘Demo()’ is ambiguous

	return 0;
}
