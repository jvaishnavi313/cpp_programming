//copy constructor scenario 2
//Returning object from function

#include<iostream>

class Demo{
	int x=10;
	int y=20;

	public:
	Demo(){
		std::cout<<"No-args constructor"<<std::endl;		
	}
	Demo(int x,int y){
		this->x=x;
		this->y=y;
		std::cout<<"Para constructor"<<std::endl;
	}
	Demo(Demo& obj){
		std::cout<<"Copy constructor"<<std::endl;
	}
	
       Demo& info(Demo& obj){                  
		obj.x=700;
		obj.y=800;
		return obj;
	}
       void access(){
		std::cout<<x<<" "<<y<<std::endl;
       }
};

int main(){
	
	Demo obj1;                          //no-args constructor
	obj1.access();

	Demo obj2(1000,2000);               //para constructor
	obj2.access();

	Demo obj3=obj2.info(obj1);
	obj3.access();

	return 0;
}
