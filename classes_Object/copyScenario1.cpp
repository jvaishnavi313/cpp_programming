//copy constructor scenario 1
//Passing object as an argument

#include<iostream>

class Demo{
	int x=10;
	int y=20;

	public:
	Demo(){
		std::cout<<"No-args constructor"<<std::endl;
		std::cout<<x<<" "<<y<<std::endl;
	}
	Demo(int x,int y){
		this->x=x;
		this->y=y;
		std::cout<<"Para constructor"<<std::endl;
		std::cout<<x<<" "<<y<<std::endl;
	}
	Demo(Demo& obj){
		std::cout<<"Copy constructor"<<std::endl;
	}
	void info(Demo obj){                     //Demo obj=obj1
		std::cout<<x<<" "<<y<<std::endl;
		std::cout<<obj.x<<" "<<obj.y<<std::endl;
	}
};

int main(){
	
	Demo obj1;                          //no-args constructor
	
	Demo obj2(1000,2000);               //para constructor
	
	obj2.info(obj1);

	return 0;
}
