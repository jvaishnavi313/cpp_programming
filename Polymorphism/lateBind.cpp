#include<iostream>

void add(int x,int y){
	std::cout<<x+y<<std::endl;
}

void sub(int x,int y){
	std::cout<<x-y<<std::endl;
}

void mul(int x,int y){
	std::cout<<x*y<<std::endl;
}

int main(){
	
	std::cout<<"1.add 2.sub 3.mul "<<std::endl;
	int ch;
	std::cout<<"Enter choice"<<std::endl;
	std::cin>>ch;

	void (*funPtr)(int, int) = NULL;

	switch(ch){
		case 1: funPtr=add;
			break;
		
		case 2: funPtr =sub;
			break;

		case 3: funPtr = mul;
			break;
	}

	funPtr(10,20);
	return 0;
}
