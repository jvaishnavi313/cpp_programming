#include<iostream>

class Demo{
	public:
	int x=10;

	Demo(){
		std::cout<<"No-args constructor"<<std::endl;
	}
	Demo(int x){
		std::cout<<"Para constructor"<<std::endl;
	}
	Demo(Demo& obj){
		std::cout<<"Copy constructor"<<std::endl;
	}

	void fun(){
		std::cout<<x<<" "<<this->x<<std::endl;
	}
};

int main(){
	
	Demo obj1;                  //no-args constructor
	
	Demo obj2=obj1;             //copy

	std::cout<<obj1.x<<std::endl;
	std::cout<<obj2.x<<std::endl;
	
	obj1.x=50;
	std::cout<<obj1.x<<std::endl;
	std::cout<<obj2.x<<std::endl;
	return 0;
}
