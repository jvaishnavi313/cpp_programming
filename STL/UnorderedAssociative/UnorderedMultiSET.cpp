//UNORERED MultiSET : duplicate data  allowed 
//sorting is not done here
//usng hash value it will display output
//Bucket size by default 11

#include<iostream>
#include<unordered_set>

int main(){
	
	std::unordered_multiset<int> s={1,11,12,3,1,2,3,5,6};
	std::unordered_set<int>::iterator itr;

	for( itr=s.begin();itr!=s.end();itr++){
		std::cout<<*itr<<std::endl;
	}

	return 0;
}
