//if-stream
#include<iostream>
#include<fstream>

int main(){
	
	std::ifstream infile("Incubator.txt");  // for ifstream file must be present otherwise it will return null
	
	std::string input;
	
	while(infile){
		getline(infile,input);
		std::cout<<input<<std::endl;
	}

	infile.close();

	return 0;

}
