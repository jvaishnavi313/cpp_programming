#include<iostream>

class Parent{
	public:
		Parent(){
			std::cout<<"Parent Constructor"<<std::endl;
		}

		virtual ~Parent(){
			std::cout<<"Parent Destructor"<<std::endl;
		}
};

class Child : public Parent{
	int *ptr=NULL;
	public:
		Child(){
			std::cout<<"Child Constructor"<<std::endl;
		}

		~Child(){
			std::cout<<"Child Destructor"<<std::endl;
			delete[] ptr;
		}
};

int main(){
	Parent *obj=new Child();

	delete obj;

	return 0 ;
}
