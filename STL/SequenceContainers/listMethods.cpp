#include<iostream>
#include<list>

int main(){
	
	std::list<int>  lst = {1,2,3,4,5};

	//1.size
	std::cout<<"Sizr= "<<lst.size()<<std::endl;
	
	//2.empty
	std::cout<<"Empty= "<<lst.empty()<<std::endl;
	
	//3.max_size
	std::cout<<"Max_size= "<<lst.max_size()<<std::endl;
	
	//4.front
	std::cout<<"front= "<<lst.front()<<std::endl;
	
	//5.back
	std::cout<<"back= "<<lst.back()<<std::endl;
	
	//6.emplace_front
	std::cout<<"Afer emplace_front= ";
	lst.emplace_front(100);
	for(auto i=lst.begin();i!=lst.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;
	
	//7.push_front
	std::cout<<"Afer push_front= ";
	lst.push_front(50);
	for(auto i=lst.begin();i!=lst.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;
	
	//8.pop_front
	std::cout<<"Afer pop_front= ";
	lst.pop_front();
	for(auto i=lst.begin();i!=lst.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;
	
	
	//9.emplace_back
	std::cout<<"Afer emplace_back= ";
	lst.emplace_back(6);
	for(auto i=lst.begin();i!=lst.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;
	
	
	//10.push_back
	std::cout<<"Afer push_back= ";
	lst.push_back(7);
	for(auto i=lst.begin();i!=lst.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;
	
	
	//11.pop_back
	std::cout<<"Afer pop_back= ";
	lst.pop_back();
	for(auto i=lst.begin();i!=lst.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;
	
	//12.splice
	std::list<int> lst2={21,22,33};
	std::list<int>::iterator itr=lst.begin();
	lst.splice(itr,lst2);
	std::cout<<"AFtr splice= ";
	for(auto i=lst.begin();i!=lst.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;
	
	//13.merge
	std::cout<<"Afer Merge= ";
	std::list<int> lst3={98,97,96};
	lst3.merge(lst);
	for(auto i=lst3.begin();i!=lst3.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;

	//14.remove
	std::cout<<"After Remove= ";
	lst3.remove(100);
	for(auto i=lst3.begin();i!=lst3.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;
	
	//15.sort
	std::cout<<"After sort= ";
	lst3.sort();
	for(auto i=lst3.begin();i!=lst3.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;
	return 0;
}
