#include<iostream>

class Parent{
	public:
	
	Parent(){
		std::cout<<"Parent Constructor"<<std::endl;		
	}

	virtual void getData(int x){                              //getData(int)
		std::cout<<"Parent GetData"<<std::endl;
	}
	
	virtual void printData(float x){                          //printData(float)
		std::cout<<"Parent printData"<<std::endl;
	}
};

class Child : public Parent{
	
	public:
	Child(){
		std::cout<<"Child Constructor"<<std::endl;
	}

	void getData(short int x){                                //getData(short int)                   
		std::cout<<"Child getData"<<std::endl;	
	}
	
	void printData(float x){                                  //printData(float)
		std::cout<<"Child printData"<<std::endl;
	}
};

int main(){

	Parent *obj = new Child();
	obj->getData(10);
	obj->printData(10.0f);
	
	//Parent& obj1=new Child();    
	//error: invalid initialization of non-const reference of type ‘Parent&’ from an rvalue of type ‘Child*
	return 0;
}
