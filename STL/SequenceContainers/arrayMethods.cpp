//array methods
#include<iostream>
#include<array>

int main(){
	std::cout<<"ARRAY METHODS: "<<std::endl;	
	std::array<int,5> arr = {1,2,3,4,5};

	//1.size
	std::cout<<"size="<<arr.size()<<std::endl;
	
	//2.max-size
	std::cout<<"max_size="<<arr.max_size()<<std::endl;
	
	//3.empty
	std::cout<<"empty= "<<arr.empty()<<std::endl;
	
	//4.front
	std::cout<<"front= "<<arr.front()<<std::endl;
	//5.back
	std::cout<<"back= "<<arr.back()<<std::endl;
	
	//6.data
	std::cout<<"data= "<<arr.data()<<std::endl;

	//7.swap
	std::cout<<"After Swap= ";
	std::array<int,5> arr2={51,52,53};
	arr.swap(arr2);
	for(auto i=arr.begin();i!=arr.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;

	//8.fill
	std::cout<<"After fill= ";
	arr.fill(1);
	for(auto i=arr.begin();i!=arr.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;
	return 0;
}
