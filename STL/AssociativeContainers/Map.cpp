//MAP: Key Value Pair            key=> first , value=> second  ( variables )
//	perform sorting
//	if have duplicate key it overlap the previous data

#include<iostream>
#include<map>

int main(){
	
	std::map<int,std::string> m1 ;
	std::map<int,std::string>::iterator itr;
	
	//Way-1
	m1[18]="virat";
	m1[7]="MSD";
	m1[45]="Rohit";
	m1[18]="virat-dup";
	for(itr=m1.begin();itr!=m1.end();itr++){
		std::cout<<itr->first<< ": ";
		std::cout<<itr->second<<std::endl;
	}

	//descending order
	std::map<int,std::string,std::greater<int>> s2;
	std::map<int,std::string>::iterator it;
	s2[18]="virat";
	s2[7]="MSD";
	s2[45]="Rohit";
	s2[18]="virat-dup";
	
	std::cout<<"Descending Order: "<<std::endl;
	for(it=s2.begin();it!=s2.end();it++){
		std::cout<<it->first<< ": ";
		std::cout<<it->second<<std::endl;
	}

	//Way-2
	
	std::map<std::string,std::string> m2={{"Vaish","Jadhav"},{"AK","Malusare"},{"ANI","Navle"}};
	std::map<std::string,std::string>::iterator i1;

	std::cout<<"WAY-2: "<<std::endl;
	for(i1=m2.begin();i1!=m2.end();i1++){
		std::cout<<i1->first<< ": ";
		std::cout<<i1->second<<std::endl;
	}
	
	//Descending only key needed
	std::map<std::string,std::string,std::greater<std::string>> m3={{"Vaish","Jadhav"},{"AK","Malusare"},{"ANI","Navle"}};
	std::map<std::string,std::string>::iterator i2;

	std::cout<<"WAY-2:Descending "<<std::endl;
	for(i2=m3.begin();i2!=m3.end();i2++){
		std::cout<<i2->first<< ": ";
		std::cout<<i2->second<<std::endl;
	}

	//Way-3
	std::map<int,std::string> player;
	player.insert(std::pair<int,std::string>(10,"Messi"));
	player.insert(std::pair<int,std::string>(7,"Ronaldo"));
	
	std::map<int,std::string>::iterator i3;
	
	std::cout<<"WAY-3: "<<std::endl;
	for(i3=player.begin();i3!=player.end();i3++){
		std::cout<<i3->first<< ": ";
		std::cout<<i3->second<<std::endl;
	}

	return 0;
}
