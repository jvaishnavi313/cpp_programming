#include<iostream>
#include<vector>

int main(){
	
	std::vector<int> v = {1,2,3,4,5};
	
	std::vector<int>::iterator itr1;
	//begin() and end()
	for(itr1=v.begin();itr1!=v.end();itr1++){
		std::cout<<*itr1<<" ";
	}
	std::cout<<std::endl;
	
	
	
	std::vector<int>::const_iterator itr2;
	//cbegin() and cend()              for constant iterator type of iterator must be const_iterator
	for(itr2=v.cbegin();itr2!=v.cend();itr2++){
		//*i=100;                             error: assignment of read-only location
		std::cout<<*itr2<<" ";
	}
	std::cout<<std::endl;
	
	
	std::vector<int>::reverse_iterator itr3;            //for reverse type of iterator must be reverse_iterator
	//rbegin() and rend()                 reverse Begin
	for(itr3=v.rbegin();itr3!=v.rend();itr3++){
		std::cout<<*itr3<<" ";
	}
	std::cout<<std::endl;
	
	
	std::vector<int>::const_reverse_iterator itr4;      //for constant reverse type of iterator must be const_reverse_iterator
	//crbegin() and crend()                constant reverse   
	for(itr4=v.crbegin();itr4!=v.crend();itr4++){
		std::cout<<*itr4<<" ";
		//*i=100;                             error: assignment of read-only location
	}
	std::cout<<std::endl;
	return 0;

}
