#include<iostream>


class Parent{
	int x=10;
	protected:
	int y=20;
	public:
	int z=30;

	Parent(){
		std::cout<<"Parent constructor"<<std::endl;
	}
};

class Child : protected  Parent{
	
	public:
	Child(){
		std::cout<<"Child constructor"<<std::endl;
	}
};
int main(){
	
	Child obj;

	std::cout<<obj.x<<std::endl;
	std::cout<<obj.y<<std::endl;
	std::cout<<obj.z<<std::endl;
	
	// error: ‘int Parent::x’ is private within this context
	// error: ‘int Parent::y’ is protected within this context
	//error: ‘int Parent::z’ is inaccessible within this context
	return 0;
}
