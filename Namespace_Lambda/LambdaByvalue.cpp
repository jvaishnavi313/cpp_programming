#include<iostream>

int main(){
	
	int a=10;
	std::string name="main";
	
	std::cout<<a<<std::endl;

	auto add=[=](int x, int y)mutable->auto{                                 // = -> all variables captured by value
		a++;
		std::cout<<a<<": "<<name<<std::endl;
		return x+y;
	};
	
	
	std::cout<<add(10,20)<<std::endl;
	std::cout<<a<<std::endl;

	return 0;
}
