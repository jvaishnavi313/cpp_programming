#include<iostream>
class Parent {
    public:
        virtual void getData(int){
            std::cout<<"Parent getData"<<std::endl;
        }
     
};

class Child : public Parent {
    public:
	/*
        void getData()override{
            std::cout<<"Child getData"<<std::endl;          //error: ‘void Child::getData()’ marked ‘override’, but does not override

        } */


        void getData(int)override{
            std::cout<<"Child getData"<<std::endl;

        } 
};
int main(){
    Parent *obj=new Child();
    obj->getData(10);
    return 0;
}
