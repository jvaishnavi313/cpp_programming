#include<iostream>
class Demo{
	int x=10;
	public:
	int y=20;
	protected:
	int z=30;	
};

int main(){
	Demo obj;
	std::cout<<obj.x<<" "<<obj.z<<std::endl; 
	//error: ‘int Demo::x’ is private within this context
	//error: ‘int Demo::z’ is protected within this context
	return 0;
}
