#include<iostream>


class Parent{
	int x=10;
	protected:
	int y=20;
	public:
	int z=30;

	Parent(){
		std::cout<<"Parent constructor"<<std::endl;
	}
};

class Child : public Parent{
	
	public:
	Child(){
		std::cout<<"Child constructor"<<std::endl;
	}
};
int main(){
	
	Child obj;

	std::cout<<obj.x<<std::endl;
	std::cout<<obj.y<<std::endl;
	
	// error: ‘int Parent::x’ is private within this context
	// error: ‘int Parent::y’ is protected within this context

	return 0;
}
