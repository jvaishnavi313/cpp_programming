//if-stream
#include<iostream>
#include<fstream>

int main(){
	
	std::ifstream infile("Incubator.txt");  
	std::ofstream outfile("Biencaps.txt");
	
	std::string input;
	
	while(infile){
		getline(infile,input);
		outfile<<input<<std::endl;
	}

	infile.close();
	outfile.close();

	return 0;

}
