#include<iostream>

class Company{
	
	int compEmp=5000;
	std::string name="IBM";

	//const char name[]="IBM";
	
	public:
	Company(){
		std::cout<<"In company constructor"<<std::endl;
	}
	void compInfo(){
	
		std::cout<<compEmp<<std::endl;
		std::cout<<name<<std::endl;
	}
};

class Employee{
	
	int empId=10;
	float empSal=56.7f;
	public:
	Employee(){
		std::cout<<"In employee constructor"<<std::endl;
	}

	void empInfo(){
	
		std::cout<<empId<<std::endl;
		std::cout<<empSal<<std::endl;
		Company obj;
		obj.compInfo();
	}
};

int main(){
	Employee *emp = new Employee();
	emp->empInfo();
	return 0 ;
}
