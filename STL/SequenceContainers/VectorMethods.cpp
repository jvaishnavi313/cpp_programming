#include<iostream>
#include<vector>

int main(){
	
	std::vector<int> v = {11,21,31,41,51};

	//1.size
	std::cout<<"Size= "<<v.size()<<std::endl;
	
	//2.max_size
	std::cout<<"Max_Size= "<<v.max_size()<<std::endl;
	
	//3.resize
	v.resize(7);
	std::cout<<"Resize= "<<v.size()<<std::endl;
	
	//4.capacity
	std::cout<<"Capacity= "<<v.capacity()<<std::endl;
	
	//5.empty
	std::cout<<"Empty= "<<v.empty()<<std::endl;
	
	//6.reserve
	v.reserve(8);
	std::cout<<"Reserve= "<<v.size()<<std::endl;
	
	//7.shrink_to_fit
	v.shrink_to_fit();
	std::cout<<"After shrink size= "<<v.size()<<std::endl;
	
	//8.operator[]
	std::cout<<"Operator[]= "<<v[4]<<std::endl;
	
	//9.empty
	std::cout<<"Empty= "<<v.empty()<<std::endl;
	
	//10.at
	std::cout<<"at= "<<v.at(1)<<std::endl;
	
	//11.front
	std::cout<<"front= "<<v.front()<<std::endl;
	
	//12.back
	std::cout<<"back= "<<v.back()<<std::endl;
	
	//13.data
	std::cout<<"data= "<<v.data()<<std::endl;                   //address of first ele

	//14.emplace
	v.emplace(v.begin(),100);
	std::cout<<"Emplace= ";
	for(int i=0;i<v.size();i++){
		std::cout<<v[i]<<" ";
	}std::cout<<std::endl;
	
	//15.emplace_back
	v.emplace_back(100);
	std::cout<<"Emplace_Back= ";
	for(int i=0;i<v.size();i++){
		std::cout<<v[i]<<" ";
	}std::cout<<std::endl;
	
	//16.erase
	v.erase(v.begin());	             //pointer required
	std::cout<<"After erase= ";
	for(int i=0;i<v.size();i++){
		std::cout<<v[i]<<" ";
	}std::cout<<std::endl;
	
	//17.clear
	v.clear();	             //pointer required
	std::cout<<"After clear= ";
	for(int i=0;i<v.size();i++){
		std::cout<<v[i]<<" ";
	}std::cout<<std::endl;
	
	//18.assign
	v.assign(3,30);
	std::cout<<"After assign= ";
	for(int i=0;i<v.size();i++){
		std::cout<<v[i]<<" ";
	}std::cout<<std::endl;

	return 0;
}
