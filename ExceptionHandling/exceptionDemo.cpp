#include<iostream>

class Demo : public std::exception{
    public:
    int x=10;
    Demo(){
        this->x=80;
        std::cout<<"In constructor"<<std::endl;
    }
    Demo(Demo& obj){
        std::cout<<"Copy consctor"<<std::endl;

    }
    ~Demo(){
        std::cout<<"In des"<<std::endl;

    }
    void getData()const{
            std::cout<<x<<std::endl;
    }
};

int main(){

    try{
        Demo obj;
        throw obj;
    }catch(Demo& ie){
        std::cout<<"Exception"<<std::endl;
    }catch(Demo& i){
    
    }catch(std::exception ie){
        std::cout<<"Exception second"<<std::endl;  
    }

    
    return 0;
}
