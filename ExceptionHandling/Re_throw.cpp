#include<iostream>

void search(int arr[],int size,int ele){
	
	int flag=0;
	int i;
	for(i=0;i<size;i++){
		if(arr[i]==ele){
			flag=1;
			break;
		}
	}

	try{
		if(flag==1){
			std::cout<<"Element found at index "<<i<<std::endl;
		}else{
			throw "Number not found";
		}
	}catch(const char* str){
		std::cout<<str<<std::endl;
		throw;
	}
}

int main(){
	int arr[]={10,20,30};
	int value;
	std::cin>>value;

	try{
		search(arr,3,value);
	}catch(const char* str){
		std::cout<<"In main: "<<str<<std::endl;
	}
	return 0;
}
