//one friend function can be friend of more than one class
//forward declaration needed

#include<iostream>

class Two;            //forward declaration
class One{
	int x=10;

	protected:
	int y=20;

	public:
	One(){
		std::cout<<"One constructor"<<std::endl;
	}

	private:
	void getData()const{
		std::cout<<"x="<<x<<" y="<<y<<std::endl;
	}

	friend void accessData(const One& obj1,const Two obj2);
};

class Two{
	int a=50;

	protected:
	int b=60;

	public:
	Two(){
		std::cout<<"Two constructor"<<std::endl;
	}

	private:
	void getData()const{
		std::cout<<"a="<<a<<" b="<<b<<std::endl;
	}

	friend void accessData(const One& obj1,const Two obj2);
};

void accessData(const One& obj1,const Two obj2){
	obj1.getData();
	obj2.getData();
}

int main(){
	One obj1;
	Two obj2;

	accessData(obj1,obj2);

	return 0;
}
