#include<iostream>
#include<deque>

int main(){
	
	std::deque<int>  dq = {1,2,3,4,5};
	
	std::cout<<"DEQUE METHODS: "<<std::endl;
	//1.size
	std::cout<<"Sizr= "<<dq.size()<<std::endl;
	
	//2.empty
	std::cout<<"Empty= "<<dq.empty()<<std::endl;
	
	//3.max_size
	std::cout<<"Max_size= "<<dq.max_size()<<std::endl;
	
	//4.front
	std::cout<<"front= "<<dq.front()<<std::endl;
	
	//5.back
	std::cout<<"back= "<<dq.back()<<std::endl;
	
	//6.emplace_front
	std::cout<<"Afer emplace_front= ";
	dq.emplace_front(100);
	for(auto i=dq.begin();i!=dq.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;
	
	//7.push_front
	std::cout<<"Afer push_front= ";
	dq.push_front(50);
	for(auto i=dq.begin();i!=dq.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;
	
	//8.pop_front
	std::cout<<"Afer pop_front= ";
	dq.pop_front();
	for(auto i=dq.begin();i!=dq.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;
	

	//9.emplace_back
	std::cout<<"Afer emplace_back= ";
	dq.emplace_back(6);
	for(auto i=dq.begin();i!=dq.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;
	
	
	//10.push_back
	std::cout<<"Afer push_back= ";
	dq.push_back(7);
	for(auto i=dq.begin();i!=dq.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;
	
	
	//11.pop_back
	std::cout<<"Afer pop_back= ";
	dq.pop_back();
	for(auto i=dq.begin();i!=dq.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;


	//12.insert
	std::cout<<"After insert = ";
	dq.insert(dq.begin(),66);                    //requied iterator or pointer
	for(auto i=dq.begin();i!=dq.end();i++) std::cout<<*i<<" ";
	std::cout<<std::endl;
	
	return 0;
}
