//fstream => both ifstream and ofstream
#include<iostream>
#include<fstream>

int main(){
	
	std::fstream infile("Incubator.txt",std::ios::in | std::ios::out); 
	
	std::string input;
	
	
	while(infile){
		getline(infile,input);
		std::cout<<input<<std::endl;
	
	}
	infile.close();
	
	infile.open("Incubator.txt",std::ios::app);
	infile<<"Devops\n";
	infile<<"Testing\n";
	infile.close();

	return 0;

}
