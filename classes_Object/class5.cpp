#include<iostream>

class Player{
	public:

	int jerNo;
	std::string name;

	void info(){
		std::cout<<jerNo<<" "<<name<<std::endl;
	}
};

int main(){
	Player vk{18,"virat"};            //if instance variable is public we can give initialiser list directly to object
	
	//Player vk{18,"virat",14.54};    //error: too many initializers for ‘Player’      
					  
	vk.info();
	vk.jerNo=45;
	vk.name="Rohit";

	vk.info();
	return 0;
}
