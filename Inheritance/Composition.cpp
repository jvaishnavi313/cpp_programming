#include<iostream>

class Employee{
	
	std::string eName="kanha";
	int empId=255;

	public:
	Employee(){
		std::cout<<"Emp Constructor"<<std::endl;
	}

	void getInfo(){
		std::cout<<eName<<std::endl;
	}
	~Employee(){
		std::cout<<"Emp destructor"<<std::endl;
	}
};

class Company{
	
	std::string cName ="veritas";
	int strEmp=100;

	Employee obj;

	public:
	Company(std::string cName,int strEmp){
		std::cout<<"comp constructor"<<std::endl;
		this->cName=cName;
		this->strEmp=strEmp;
	}

	void getInfo(){
		std::cout<<cName<<" "<<strEmp<<std::endl;
		obj.getInfo();
	}

	~Company(){
		std::cout<<"Comp destructor"<<std::endl;
	}
};

int main(){
	
	Company obj("Pubmatic",5000);
	obj.getInfo();

	return 0 ;
}
