#include<iostream>
class Parent{

    int x=10;
    protected:
    int y=20;
    public:
    int z=30;

    void getData(){
        std::cout<<x<<y<<z<<std::endl;
    }

    Parent(int x,int y,int z){
        std::cout<<"IN  Parent para"<<std::endl;
            this->x=x;
            this->y=y;
            this->z=z;
    }
    Parent(){
        std::cout<<"IN no para"<<std::endl;
    }

};

class child : Parent{

    public:
  
    child(int x,int y,int z){
        
            std::cout<<"IN para child"<<std::endl;
            Parent(x,y,z);
            

        }
        void getInfo(){

            std::cout<<y<<z<<std::endl;
            getData();
        }


        
};

int main(){

    child obj(80,90,100);
    obj.getInfo();
 
    return 0;
}
