#include<iostream>

class Demo{
	public:
	Demo(){
		std::cout<<"No-args constructor"<<std::endl;
	}
	Demo(int x){
		std::cout<<"Para constructor"<<std::endl;
	}
	Demo(Demo& obj){
		std::cout<<"Copy constructor"<<std::endl;
	}
};

int main(){
	
	Demo obj1;                  //no-args constructor
	
	Demo obj2(10);               //para constructor
	
	//in below both call for copy
	Demo obj3(obj1);            //copy constructor
	Demo obj4 = obj1;           //copy constructor

	return 0;
}
